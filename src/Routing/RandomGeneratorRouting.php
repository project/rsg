<?php

namespace Drupal\rsg\Routing;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\rsg\RandomGenerator;

/**
 * Routing for the Random Generation.
 */
class RandomGeneratorRouting implements ContainerInjectionInterface {

  /**
   * Random service.
   *
   * @var \Drupal\rsg\RandomGenerator
   */
  protected $randomGenerator;

  /**
   * Constructs an RandomGenerator object.
   *
   * @param \Drupal\rsg\RandomGenerator $random_generator
   *   To generate the rsg.
   */
  public function __construct(RandomGenerator $random_generator) {
    $this->randomGenerator = $random_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('random.string.generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routeCollection = new RouteCollection();
    $collection = [
      'string' => 'String',
      'word' => 'Word',
      'wordSmall' => 'Word Small',
      'wordSmallNumeric' => 'Word Small Numeric',
      'wordCapital' => 'Word Capital',
      'wordCapitalNumeric' => 'Word Capital Numeric',
      'name' => 'Name',
      'number' => 'Number',
      'generate' => 'Generate',
    ];
    $url = [
      'string' => 'string',
      'word' => 'word',
      'wordSmall' => 'word-small',
      'wordSmallNumeric' => 'word-small-numeric',
      'wordCapital' => 'word-capital',
      'wordCapitalNumeric' => 'word-capital-numeric',
      'name' => 'name',
      'number' => 'number',
      'generate' => 'generate',
    ];
    foreach ($collection as $key => $name) {
      $path = '/random/' . $url[$key] . '/{length}';
      if ($key == 'name' || $key == 'generate') {
        $path .= '/{source}';
      }
      switch ($key) {
        case 'name':
          $source = $this->randomGenerator->getDefaultName();
          break;

        case 'generate':
          $source = $this->randomGenerator->getDefaultGenerate();
          break;

        default:
          $source = '';
          break;
      }

      $route = new Route(
        $path,
        [
          '_controller' => '\Drupal\rsg\Controller\RandomGeneratorController::' . $key,
          '_title' => $name . ' Generator',
          'length' => $this->randomGenerator->getLength(),
          'source' => $source,
        ],
        [
          '_permission' => 'access content',
          '_format' => 'json',
        ],
        [
          'no_cache' => TRUE,
        ],
        NULL,
        [],
        ['post'],
      );
      $routeCollection->add('random.generator.' . $key, $route);
    }
    return $routeCollection;
  }

}
