<?php

namespace Drupal\rsg\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\rsg\RandomGenerator;

/**
 * RandomGeneratorController provides the routing for the rsg.
 */
class RandomGeneratorController extends ControllerBase {

  /**
   * Random service.
   *
   * @var \Drupal\rsg\RandomGenerator
   */
  protected $randomGenerator;

  /**
   * Constructs an RandomGenerator object.
   *
   * @param \Drupal\rsg\RandomGenerator $random_generator
   *   To generate the rsg.
   */
  public function __construct(RandomGenerator $random_generator) {
    $this->randomGenerator = $random_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('random.string.generator')
    );
  }

  /**
   * This will generate the String with alpha numberic.
   *
   * @return string
   *   will return the string generated.
   */
  public function string($length = "") {
    return new JsonResponse([$this->randomGenerator->string($length)]);
  }

  /**
   * This will generate the String with alphabets.
   *
   * @return string
   *   will return the word generated.
   */
  public function word($length = "") {
    return new JsonResponse([$this->randomGenerator->word($length)]);
  }

  /**
   * This will generate the String with small alphabets.
   *
   * @return string
   *   will return the Word Small generated.
   */
  public function wordSmall($length = "") {
    return new JsonResponse([$this->randomGenerator->wordSmall($length)]);
  }

  /**
   * This will generate the String with small alphabets with numbers.
   *
   * @return string
   *   will return the word Small Numeric generated.
   */
  public function wordSmallNumeric($length = "") {
    return new JsonResponse([$this->randomGenerator->wordSmallNumeric($length)]);
  }

  /**
   * This will generate the String with capital alphabets.
   *
   * @return string
   *   will return the Word Capital generated.
   */
  public function wordCapital($length = "") {
    return new JsonResponse([$this->randomGenerator->wordCapital($length)]);
  }

  /**
   * This will generate the String with capital alphabets with numbers.
   *
   * @return string
   *   will return the Word Capital Numeric generated.
   */
  public function wordCapitalNumeric($length = "") {
    return new JsonResponse([$this->randomGenerator->wordCapitalNumeric($length)]);
  }

  /**
   * This will generate the String with name and small alphabets.
   *
   * @return string
   *   will return the name generated.
   */
  public function name($length = "", $source = 'username') {
    return new JsonResponse([$this->randomGenerator->name($length, $source)]);
  }

  /**
   * This will generate the String with numeric.
   *
   * @return string
   *   will return the number generated.
   */
  public function number($length = "") {
    return new JsonResponse([$this->randomGenerator->number($length)]);
  }

  /**
   * This will generate the String with external string.
   *
   * @return string
   *   will return the generated value.
   */
  public function generate($length = "", $source = '') {
    return new JsonResponse([$this->randomGenerator->generate($length, $source)]);
  }

}
