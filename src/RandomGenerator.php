<?php

namespace Drupal\rsg;

/**
 * RandomGenerator is a service provider for rsg.
 */
class RandomGenerator {

  /**
   * String contains only small alphabets.
   *
   * @var string
   */
  const SMALL_ALPHABETS = 'abcdefghijklmnopqrstuvwxyz';

  /**
   * String contains only BIG alphabets.
   *
   * @var string
   */
  const BIG_ALPHABETS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  /**
   * String contains only Numeric.
   *
   * @var string
   */
  const NUMERIC = '0123456789';

  /**
   * String contains username.
   *
   * @var string
   */
  const USERNAME = 'username';

  /**
   * String contains only Small Alphabets With Numerics.
   *
   * @var string
   */
  const SMALL_ALPHA_NUMERIC = self::NUMERIC . self::SMALL_ALPHABETS;

  /**
   * String contains only Big Alphabets With Numerics.
   *
   * @var string
   */
  const BIG_ALPHA_NUMERIC = self::NUMERIC . self::BIG_ALPHABETS;

  /**
   * String contains only Alphabets With Numerics.
   *
   * @var string
   */
  const ALPHA_NUMERIC = self::NUMERIC . self::SMALL_ALPHABETS . self::BIG_ALPHABETS;

  /**
   * Length of the String.
   *
   * @var string
   */
  const LENGTH = 8;

  /**
   * This will return the default length.
   */
  public function getLength() {
    return self::LENGTH;
  }

  /**
   * This will return the default name value.
   */
  public function getDefaultName() {
    return self::USERNAME;
  }

  /**
   * This will return the default generate value.
   */
  public function getDefaultGenerate() {
    return self::ALPHA_NUMERIC;
  }

  /**
   * This will generate the String with alpha numberic.
   *
   * @return string
   *   will return the string generated.
   */
  public function string(int $length = self::LENGTH) {
    return $this->developString($length, self::ALPHA_NUMERIC);
  }

  /**
   * This will generate the String with alphabets.
   *
   * @return string
   *   will return the word generated.
   */
  public function word(int $length = self::LENGTH) {
    return $this->developString($length, self::SMALL_ALPHABETS . self::BIG_ALPHABETS);
  }

  /**
   * This will generate the String with small alphabets.
   *
   * @return string
   *   will return the word small generated.
   */
  public function wordSmall(int $length = self::LENGTH) {
    return $this->developString($length, self::SMALL_ALPHABETS);
  }

  /**
   * This will generate the String with small alphabets with numbers.
   *
   * @return string
   *   will return the word small numeric generated.
   */
  public function wordSmallNumeric(int $length = self::LENGTH) {
    return $this->developString($length, self::SMALL_ALPHA_NUMERIC);
  }

  /**
   * This will generate the String with capital alphabets.
   *
   * @return string
   *   will return the word capital generated.
   */
  public function wordCapital(int $length = self::LENGTH) {
    return $this->developString($length, self::BIG_ALPHABETS);
  }

  /**
   * This will generate the String with capital alphabets with numbers.
   *
   * @return string
   *   will return the word capital number generated.
   */
  public function wordCapitalNumeric(int $length = self::LENGTH) {
    return $this->developString($length, self::BIG_ALPHA_NUMERIC);
  }

  /**
   * This will generate the String with name and small alphabets.
   *
   * @return string
   *   will return the name generated.
   */
  public function name(int $length = self::LENGTH, $name = self::USERNAME) {
    return $name . '_' . $this->developString($length, self::SMALL_ALPHABETS);
  }

  /**
   * This will generate the String with numeric.
   *
   * @return string
   *   will return the number generated.
   */
  public function number(int $length = self::LENGTH) {
    return $this->developString($length, self::NUMERIC);
  }

  /**
   * This will generate the String with external string.
   *
   * @return string
   *   will return the generated value.
   */
  public function generate(int $length = self::LENGTH, string $source = self::ALPHA_NUMERIC) {
    return $this->developString($length, $source);
  }

  /**
   * This will be used internally to generate the string or number.
   */
  private function developString($length, $source): string {
    if ($length < 1) {
      throw new \RangeException(t("Length must be a positive integer"));
    }
    $chunk = [];
    $maxLength = mb_strlen($source, 'UTF-8') - 1;
    for ($i = 0; $i < $length; ++$i) {
      $chunk[] = $source[random_int(0, $maxLength)];
    }
    return implode('', $chunk);
  }

}
