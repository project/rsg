(function ($, Drupal, once, drupalSettings) {
  Drupal.behaviors.randomGenerator = {
    attach: function (context, settings) {
      once('randomGenerator', 'html', context).forEach(function (element) {
        // To generate String.
        Drupal.generateString = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/string/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate Word.
        Drupal.generateWord = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/word/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate word with small letters.
        Drupal.generateWordSmall = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/word-small/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the word with small letters and numbers.
        Drupal.generateWordSmallNumeric = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/word-small-numeric/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the word with capital letters
        Drupal.generateWordCapital = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/word-capital/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the word with capital letters and numbers.
        Drupal.generateWordCapitalNumeric = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/word-capital-numeric/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the name like username...
        Drupal.generateName = function (length = drupalSettings.randomGenerator.lengthDefault, name = drupalSettings.randomGenerator.nameDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/name/" + length + '/' + name,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the numbers.
        Drupal.generateNumber = function (length = drupalSettings.randomGenerator.lengthDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/number/" + length,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
        // To generate the word with given details.
        Drupal.generateRandom = function (length = drupalSettings.randomGenerator.lengthDefault, source = drupalSettings.randomGenerator.sourceDefault) {
          var value = '';
          $.ajax({
            'url' : "/random/generate/" + length + '/' + source,
            'type' : 'post',
            'async': false,
            'success': function (data) {
              value = data[0];
            }
          });
          return value;
        };
      });
    }
  };
})(jQuery, Drupal, once, drupalSettings);