CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers


INTRODUCTION
------------

Random String Generator module allows the developer to randomly generate
the string or a word or a number that will allows the user to use those
randomly generated key.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/rsg

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/rsg



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See https://www.drupal.org/node/895232 for further information.
   
 * By Using composer, composer require "drupal/rsg"

USAGE
-----------

 * There will be as a service that will be helpful to generate
   the string as below.
   - \Drupal::service('random.string.generator')->string(10);
   - \Drupal::service('random.string.generator')->word(10);
   - \Drupal::service('random.string.generator')->number(10);
 * This will be used in routes that will help to generate the random content
   - /random/string/10
   - /random/word/10
   - /random/word-small/10
   - /random/word-small-numeric/10
   - /random/word-capital/10
   - /random/word-capital-numeric/10
   - /random/name/10/username
   - /random/number/10
   - /random/generate/10/pattern
 * This will be used in JS to generate and use the strings.
   - Drupal.generateString(10)
   - Drupal.generateWord(10)
   - Drupal.generateWordSmall(10)
   - Drupal.generateWordSmallNumeric(10)
   - Drupal.generateWordCapital(10)
   - Drupal.generateWordCapitalNumeric(10)
   - Drupal.generateName(10, 'username')
   - Drupal.generateNumber(10)
   - Drupal.generateRandom(10, 'patterns')

MAINTAINERS
-----------

Current maintainers:
 * Praveen Achanta (praveen3555) - https://www.drupal.org/u/praveen3555
